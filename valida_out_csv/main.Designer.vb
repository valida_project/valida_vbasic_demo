﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class main
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(main))
        Me.cbports = New System.Windows.Forms.ComboBox()
        Me.openPortButton = New System.Windows.Forms.Button()
        Me.SerialGroupBox = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BRtextbox = New System.Windows.Forms.TextBox()
        Me.closePortButton = New System.Windows.Forms.Button()
        Me.openCloseLabel = New System.Windows.Forms.Label()
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.numRxTextBox = New System.Windows.Forms.TextBox()
        Me.sendTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.RxdataLabel = New System.Windows.Forms.Label()
        Me.AnswerTextBox = New System.Windows.Forms.TextBox()
        Me.BorrarButton = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SerialGroupBox.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbports
        '
        Me.cbports.FormattingEnabled = True
        Me.cbports.Items.AddRange(New Object() {"COM8"})
        Me.cbports.Location = New System.Drawing.Point(9, 22)
        Me.cbports.Name = "cbports"
        Me.cbports.Size = New System.Drawing.Size(66, 21)
        Me.cbports.TabIndex = 0
        '
        'openPortButton
        '
        Me.openPortButton.BackColor = System.Drawing.SystemColors.Control
        Me.openPortButton.Location = New System.Drawing.Point(190, 16)
        Me.openPortButton.Name = "openPortButton"
        Me.openPortButton.Size = New System.Drawing.Size(63, 31)
        Me.openPortButton.TabIndex = 1
        Me.openPortButton.Text = "ABRIR"
        Me.openPortButton.UseVisualStyleBackColor = False
        '
        'SerialGroupBox
        '
        Me.SerialGroupBox.Controls.Add(Me.Label2)
        Me.SerialGroupBox.Controls.Add(Me.BRtextbox)
        Me.SerialGroupBox.Controls.Add(Me.closePortButton)
        Me.SerialGroupBox.Controls.Add(Me.openCloseLabel)
        Me.SerialGroupBox.Controls.Add(Me.cbports)
        Me.SerialGroupBox.Controls.Add(Me.openPortButton)
        Me.SerialGroupBox.Location = New System.Drawing.Point(10, 393)
        Me.SerialGroupBox.Name = "SerialGroupBox"
        Me.SerialGroupBox.Size = New System.Drawing.Size(432, 56)
        Me.SerialGroupBox.TabIndex = 2
        Me.SerialGroupBox.TabStop = False
        Me.SerialGroupBox.Text = "Puerto Serial"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Location = New System.Drawing.Point(81, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 20)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "BR"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BRtextbox
        '
        Me.BRtextbox.Location = New System.Drawing.Point(128, 22)
        Me.BRtextbox.Name = "BRtextbox"
        Me.BRtextbox.Size = New System.Drawing.Size(56, 20)
        Me.BRtextbox.TabIndex = 24
        Me.BRtextbox.Text = "230400"
        '
        'closePortButton
        '
        Me.closePortButton.BackColor = System.Drawing.SystemColors.Control
        Me.closePortButton.Enabled = False
        Me.closePortButton.Location = New System.Drawing.Point(259, 16)
        Me.closePortButton.Name = "closePortButton"
        Me.closePortButton.Size = New System.Drawing.Size(66, 31)
        Me.closePortButton.TabIndex = 3
        Me.closePortButton.Text = "CERRAR"
        Me.closePortButton.UseVisualStyleBackColor = False
        '
        'openCloseLabel
        '
        Me.openCloseLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.openCloseLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.openCloseLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.openCloseLabel.Location = New System.Drawing.Point(331, 16)
        Me.openCloseLabel.Name = "openCloseLabel"
        Me.openCloseLabel.Size = New System.Drawing.Size(95, 31)
        Me.openCloseLabel.TabIndex = 2
        Me.openCloseLabel.Text = "Cerrado"
        Me.openCloseLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SerialPort1
        '
        Me.SerialPort1.BaudRate = 38400
        Me.SerialPort1.ReadTimeout = 200
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(12, 129)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(113, 31)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "CONFIG"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label16.Location = New System.Drawing.Point(92, 106)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(32, 20)
        Me.Label16.TabIndex = 10
        Me.Label16.Text = "Hz"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.MintCream
        Me.Label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label17.Location = New System.Drawing.Point(13, 92)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(111, 14)
        Me.Label17.TabIndex = 9
        Me.Label17.Text = "Offset Frec"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label18
        '
        Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label18.Location = New System.Drawing.Point(93, 73)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(32, 20)
        Me.Label18.TabIndex = 4
        Me.Label18.Text = "MHz"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(14, 73)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(79, 20)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Text = "432.000"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(12, 106)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(79, 20)
        Me.TextBox2.TabIndex = 8
        Me.TextBox2.Text = "7600"
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.Color.MintCream
        Me.Label20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label20.Location = New System.Drawing.Point(14, 56)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(111, 14)
        Me.Label20.TabIndex = 6
        Me.Label20.Text = "Frec Beep"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(14, 35)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(111, 20)
        Me.TextBox3.TabIndex = 5
        Me.TextBox3.Text = "0"
        Me.TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.Color.MintCream
        Me.Label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label21.Location = New System.Drawing.Point(14, 21)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(111, 14)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "ID deseado"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.numRxTextBox)
        Me.Panel1.Controls.Add(Me.sendTextBox)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.SerialGroupBox)
        Me.Panel1.Location = New System.Drawing.Point(2, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(449, 456)
        Me.Panel1.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Location = New System.Drawing.Point(320, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(116, 20)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "RxBytesThreshold"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label1.Visible = False
        '
        'numRxTextBox
        '
        Me.numRxTextBox.Location = New System.Drawing.Point(356, 37)
        Me.numRxTextBox.Name = "numRxTextBox"
        Me.numRxTextBox.Size = New System.Drawing.Size(39, 20)
        Me.numRxTextBox.TabIndex = 24
        Me.numRxTextBox.Text = "1"
        Me.numRxTextBox.Visible = False
        '
        'sendTextBox
        '
        Me.sendTextBox.Location = New System.Drawing.Point(356, 126)
        Me.sendTextBox.Name = "sendTextBox"
        Me.sendTextBox.Size = New System.Drawing.Size(39, 20)
        Me.sendTextBox.TabIndex = 23
        Me.sendTextBox.Text = "10"
        Me.sendTextBox.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.RxdataLabel)
        Me.GroupBox2.Controls.Add(Me.AnswerTextBox)
        Me.GroupBox2.Controls.Add(Me.BorrarButton)
        Me.GroupBox2.Location = New System.Drawing.Point(10, 11)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(304, 376)
        Me.GroupBox2.TabIndex = 22
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Input console"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Location = New System.Drawing.Point(9, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 16)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "RxData"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'RxdataLabel
        '
        Me.RxdataLabel.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.RxdataLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RxdataLabel.ForeColor = System.Drawing.Color.Black
        Me.RxdataLabel.Location = New System.Drawing.Point(70, 16)
        Me.RxdataLabel.Name = "RxdataLabel"
        Me.RxdataLabel.Size = New System.Drawing.Size(52, 16)
        Me.RxdataLabel.TabIndex = 19
        Me.RxdataLabel.Text = "0"
        Me.RxdataLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AnswerTextBox
        '
        Me.AnswerTextBox.Location = New System.Drawing.Point(9, 35)
        Me.AnswerTextBox.Multiline = True
        Me.AnswerTextBox.Name = "AnswerTextBox"
        Me.AnswerTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.AnswerTextBox.Size = New System.Drawing.Size(287, 296)
        Me.AnswerTextBox.TabIndex = 4
        '
        'BorrarButton
        '
        Me.BorrarButton.BackColor = System.Drawing.SystemColors.Control
        Me.BorrarButton.Location = New System.Drawing.Point(9, 337)
        Me.BorrarButton.Name = "BorrarButton"
        Me.BorrarButton.Size = New System.Drawing.Size(287, 31)
        Me.BorrarButton.TabIndex = 6
        Me.BorrarButton.Text = "BORRAR"
        Me.BorrarButton.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Button2.Location = New System.Drawing.Point(341, 152)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(71, 20)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "send"
        Me.Button2.UseVisualStyleBackColor = False
        Me.Button2.Visible = False
        '
        'main
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(451, 459)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "main"
        Me.Text = "Valida Rx CSV"
        Me.SerialGroupBox.ResumeLayout(False)
        Me.SerialGroupBox.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbports As System.Windows.Forms.ComboBox
    Friend WithEvents openPortButton As System.Windows.Forms.Button
    Friend WithEvents SerialGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents openCloseLabel As System.Windows.Forms.Label
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents closePortButton As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents AnswerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BorrarButton As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents RxdataLabel As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents sendTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BRtextbox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents numRxTextBox As System.Windows.Forms.TextBox

End Class

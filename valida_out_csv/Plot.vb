﻿Module Plot


    Public Sub plotGrid(ByVal PictureBox As System.Windows.Forms.PictureBox, ByVal color As System.Drawing.Color, ByVal minXplot As Integer, ByVal maxXplot As Integer, ByVal minYplot As Integer, ByVal maxYplot As Integer, ByVal divXplot As Integer, ByVal divYplot As Integer)
        Dim dibujo As Graphics = PictureBox.CreateGraphics 'declaramos area de dibujo 
        Dim gridPen As New Pen(color) 'pen de grillay borde
        ' Create font and brush.
        Const FONTSIZE As Integer = 8
        Const FONTSIZECENTER As Integer = 6
        Dim drawFont As New Font("Arial", FONTSIZE)
        Dim drawBrush As New SolidBrush(color)
        ' ancho de lineas indicadores de grilla
        Const LINE_LENGHT_GRID_DIV As Integer = 3
        Dim xmin, ymin, xmax, ymax, xcenter, ycenter As Integer
        Dim i, tmp, tmp2 As Integer
        Dim xScale, yScale As Double
        ' define el borde de la grafica
        xmin = 25
        ymin = 20
        ' maximo preliminar
        xmax = PictureBox.Width - 20
        ymax = PictureBox.Height - 20
        ' halla cuantos pixel equivale la unidad de fragmentacion en X
        ' para poder pone run valor a xmax sea un numero divisible entre
        ' xScale y no de errores al graficar
        xScale = Int((xmax - xmin) / (maxXplot - minXplot)) ' diferencia en pixels/ diff en datos
        xmax = xmin + xScale * (maxXplot - minXplot)
        ' halla cuantos pixel equivale la unidad de fragmentacion en Y
        ' para poder poner un valor a xmax sea un numero divisible entre
        ' yScale y no de errores al graficar
        yScale = ((ymax - ymin) / (maxYplot - minYplot)) ' diferencia en pixels/ diff en datos
        ymax = ymin + CInt(yScale * (maxYplot - minYplot)) + 3

        xcenter = xmax / 2
        ycenter = ymax / 2
        ' dibuja cuadro de grafica
        dibujo.DrawRectangle(gridPen, xmin, ymin, xmax - xmin, ymax - ymin)
        ' dibuja lineas divisoras eje Y con su string       
        tmp2 = divYplot ' string de cada division
        tmp = yScale * divYplot ' numero de pixels de cada division
        For i = 0 To ((maxYplot - minYplot) / divYplot)
            dibujo.DrawLine(gridPen, xmin - LINE_LENGHT_GRID_DIV, ymin + tmp * i, xmin + LINE_LENGHT_GRID_DIV, ymin + tmp * i)
            dibujo.DrawString(Str(maxYplot - tmp2 * i), drawFont, drawBrush, xmin - LINE_LENGHT_GRID_DIV - FONTSIZECENTER * Len(Str(maxYplot - tmp2 * i)), (ymin + tmp * i) - FONTSIZECENTER)
        Next
        ' dibuja lineas divisoras eje X con su string        
        tmp2 = divXplot ' string de cada division
        tmp = xScale * divXplot ' numero de pixels de cada division
        For i = 0 To ((maxXplot - minXplot) / divXplot)
            dibujo.DrawLine(gridPen, xmin + tmp * i, ymax - LINE_LENGHT_GRID_DIV, xmin + tmp * i, ymax + LINE_LENGHT_GRID_DIV)
            dibujo.DrawString(Str(minXplot + tmp2 * i), drawFont, drawBrush, xmin + tmp * i - FONTSIZE / 2 * Len(Str(minXplot + tmp2 * i)), ymax + LINE_LENGHT_GRID_DIV + 1)
        Next
    End Sub

    '-------------------------------------------------------------------
    ' Dibuja Altura solo valores positivos
    '-------------------------------------------------------------------
    Public Sub plotVarXYAlt(ByVal PictureBox As System.Windows.Forms.PictureBox, ByVal numberData As Integer, ByVal y() As Integer, ByVal color As System.Drawing.Color, ByVal minXplot As Integer, ByVal maxXplot As Integer, ByVal minYplot As Integer, ByVal maxYplot As Integer, ByVal divXplot As Integer, ByVal divYplot As Integer)
        Dim dibujo As Graphics = PictureBox.CreateGraphics 'declaramos area de dibujo 
        Dim gridPen As New Pen(color.White) 'pen de grillay borde
        Dim dataPen As New Pen(color) ' pen de datos
        ' Create font and brush.
        Const FONTSIZE As Integer = 8
        Dim drawFont As New Font("Arial", FONTSIZE)
        Dim drawBrush As New SolidBrush(color.White)
        ' ancho de lineas indicadores de grilla
        Dim xmin, ymin, xmax, ymax, xcenter, ycenter, xScale As Integer
        Dim ycero As Integer
        ' escala de x Y
        Dim yScale, tmpDouble As Double
        Dim i, tmp, tmp2 As Integer
        Dim tmpD As Double
        ' define el borde de la grafica
        xmin = 25
        ymin = 20
        xmax = PictureBox.Width - 20
        ymax = PictureBox.Height - 20
        ' halla cuantos pixel equivale la unidad de fragmentacion en X
        ' para poder pone run valor a xmax sea un numero divisible entre
        ' xScale y no de errores al graficar
        xScale = Int((xmax - xmin) / (maxXplot - minXplot)) ' diferencia en pixels/ diff en datos
        xmax = xmin + xScale * (maxXplot - minXplot)
        ' halla cuantos pixel equivale la unidad de fragmentacion en Y
        ' para poder poner un valor a ymax sea un numero divisible entre
        ' yScale y no de errores al graficar
        yScale = ((ymax - ymin) / (maxYplot - minYplot)) ' diferencia en pixels/ diff en datos
        ymax = ymin + yScale * (maxYplot - minYplot)
        xcenter = xmax / 2
        ycenter = ymax / 2

        ' Calcula lineas divisoras eje Y para hallar el 0 de Y
        ' ya que en el eje Y pueden haber valores positivos y negativos
        tmp2 = divYplot
        'tmp = yScale * divYplot
        tmpD = yScale * divYplot
        For i = 0 To ((maxYplot - minYplot) / divYplot)
            If (maxYplot - tmp2 * i) = 0 Then ' halla el 0
                ycero = CInt(ymin + tmpD * i)
            End If
        Next
        If ycero < ymin Then
            ycero = ymax
        End If
        ' grafica las muestras
        Try
            For i = 1 To numberData
                ' halla punto anterior
                If (y(i - 1) > maxYplot) Then
                    tmp = maxYplot
                ElseIf (y(i - 1) < minYplot) Then
                    tmp = minYplot
                Else
                    tmp = y(i - 1)
                End If
                tmpDouble = (tmp - minYplot) * yScale
                tmp = Int(tmpDouble)

                ' halla punto actual
                If (y(i) > maxYplot) Then
                    tmp2 = maxYplot
                ElseIf (y(i) < minYplot) Then
                    tmp2 = minYplot
                Else
                    tmp2 = y(i)
                End If
                tmpDouble = (tmp2 - minYplot) * yScale
                tmp2 = Int(tmpDouble)
                dibujo.DrawLine(dataPen, xmin + (i - 1) * xScale, ycero - tmp, xmin + (i) * xScale, ycero - tmp2)
            Next
        Catch ex As Exception

        End Try
    End Sub

    '------------------------------------------------------------------
    ' Limpia la pantalla de plot
    '------------------------------------------------------------------
    Public Sub clearPictureBox(ByVal PictureBox As System.Windows.Forms.PictureBox)
        Dim dibujo As Graphics = PictureBox.CreateGraphics 'declaramos area de dibujo 
        dibujo.Clear(Color.Black)
    End Sub

    '------------------------------------------------------------------
    ' Muestra en un picbox una imagen de 28x28
    '------------------------------------------------------------------
    Public Sub display28x28(ByRef img() As Byte, ByVal init As Integer, ByVal PictureBox As System.Windows.Forms.PictureBox)
        Dim dibujo As Graphics = PictureBox.CreateGraphics 'declaramos area de dibujo 
        Dim Pencil As New Pen(Brushes.Black) 'pen de grillay borde
        Dim i, j, punt As Integer
        Dim xScale, yScale As Integer
        Dim colorPix As Integer
       
        xScale = PictureBox.Width / 28
        yScale = PictureBox.Height / 28

        punt = init
        For i = 0 To 27
            For j = 0 To 27
                colorPix = img(punt)
                Dim drawBrush As New SolidBrush(Color.FromArgb(colorPix, colorPix, colorPix))
                dibujo.FillRectangle(drawBrush, j * xScale, i * yScale, xScale, yScale)
                punt = punt + 1
            Next
        Next
    End Sub


End Module

﻿Imports System.Threading
Imports System.IO
Imports System.Text.RegularExpressions
Public Class main
    Public FileName As String = ""
    '=============================================================================================================================================
    '
    '                                        CARGA DE FORMULARIO
    '
    '=============================================================================================================================================
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CheckForIllegalCrossThreadCalls = False ' esta instruccion se usa para que permita usar variables globales en el evento serial
        cbports.Items.Clear() 'limpia los items del combo box

        SerialPort1.ReceivedBytesThreshold = Val(numRxTextBox.Text())
        ' Busca puertos disponibles en pc
        For Each puertoDisponible As String In My.Computer.Ports.SerialPortNames
            cbports.Items.Add(puertoDisponible)
        Next
        ' si hay puertos disponibles
        If cbports.Items.Count > 0 Then
            cbports.Text = cbports.Items(0)
        Else
            MsgBox("NO SE ENCONTRO NINGUN PUERTO")
        End If
    End Sub

    Public Sub txt_file_save(ByVal fileName As String, ByVal fileStr As String)
        Dim writer As StreamWriter

        writer = File.AppendText(fileName)
        writer.Write(fileStr)
        writer.Flush()
        writer.Close()
    End Sub



    '--------------------------------------------------
    ' Evento de Llegada Serial
    '--------------------------------------------------
    Private Sub SerialPort1_DataReceived(ByVal sender As System.Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles SerialPort1.DataReceived
        Dim numBytes, i As Integer
        Dim tramaReceived(1000) As Byte
        Dim debugTrama As String = ""
        Dim AnswerTrama As String = ""
        Dim ErrorTrama As String = ""
        Dim tmpStr As String = ""
        Dim DebugString As String = ""
        Dim numBytes2wait As Integer

        numBytes2wait = SerialPort1.ReceivedBytesThreshold
        numBytes = SerialPort1.BytesToRead ' lee el numero de bytes que llegaron
        'numBytes = 64
        'numBytes2wait = 64

        RxdataLabel.Text = RxdataLabel.Text + numBytes
        If numBytes < numBytes2wait Then
            GoTo out_isr
        End If

        Try
            For i = 0 To numBytes - 1
                tramaReceived(i) = SerialPort1.ReadByte ' Guarda  todo lo que hay en el buffer
                AnswerTrama = AnswerTrama & tramaReceived(i) & ","

            Next i
            txt_file_save(FileName, AnswerTrama)
        Catch ex As Exception
            SerialPort1.ReadExisting()
            GoTo out_isr
        End Try



show_AnswerTrama:

        'dibuja en text de answer
        If AnswerTrama <> "" Then
            AnswerTextBox.AppendText(AnswerTrama)
            AnswerTextBox.Select(AnswerTextBox.Text.Length + 100, 0) ' muestra hasta ultima linea
        End If

out_isr:
    End Sub

    '--------------------------------------------------
    ' Boton para abrir  puerto serial
    '--------------------------------------------------
    Private Sub openPortButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openPortButton.Click
        Dim trama(64) As Byte
        Try
            SerialPort1.ReceivedBytesThreshold = Val(numRxTextBox.Text())
            SerialPort1.BaudRate = Val(BRtextbox.Text)
            SerialPort1.PortName = cbports.Text
            SerialPort1.Open()
            openCloseLabel.Text = "Abierto"
            openCloseLabel.BackColor = Color.YellowGreen
            closePortButton.Enabled = True
            openPortButton.Enabled = False

            FileName = Year(Today).ToString.PadLeft(4, "0") &
                        Month(Today).ToString.PadLeft(2, "0") &
                        Microsoft.VisualBasic.DateAndTime.Day(Today).ToString.PadLeft(2, "0") &
                        Hour(Now).ToString.PadLeft(2, "0") &
                        Minute(Now).ToString.PadLeft(2, "0") &
                        Second(Now).ToString.PadLeft(2, "0") &
                        "_valida_out.csv"


        Catch ex As Exception
            MsgBox("Error Abriendo el Puerto")
        End Try
    End Sub
    '--------------------------------------------------
    ' Boton para cerrar  puerto serial
    '--------------------------------------------------
    Private Sub closePortButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles closePortButton.Click
        If SerialPort1.IsOpen Then
            closePortButton.Enabled = False
            openPortButton.Enabled = True
            openCloseLabel.Text = "Cerrado"
            openCloseLabel.BackColor = Color.WhiteSmoke
            SerialPort1.Close()
        End If
    End Sub
    

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim trama(64) As Byte
        trama(0) = sendTextBox.Text

        SerialPort1.Write(trama, 0, 0 + 1)
        'Cambia le numero de bytes que espera recibir
        ' SerialPort1.ReceivedBytesThreshold = 5
    End Sub

   
    Private Sub numRxTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles numRxTextBox.TextChanged
        SerialPort1.ReceivedBytesThreshold = Val(numRxTextBox.Text())
    End Sub

    Private Sub BorrarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BorrarButton.Click
        AnswerTextBox.Text = ""
        RxdataLabel.Text = "0"
    End Sub
End Class




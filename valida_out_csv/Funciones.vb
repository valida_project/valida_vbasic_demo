﻿Module Funciones
    '=============================================================================================================================================
    '
    '                                        FUNCIONES GENERALES
    '
    '=============================================================================================================================================
    ' Limpia un buffer
    Public Sub clearBuffer(ByRef buf() As Integer, ByVal size As Integer)
        For i = 0 To size - 1
            buf(i) = 0
        Next
    End Sub

    

    'copia un buffer
    Public Sub BufferPasteDbl(ByRef bufFrom() As Double, ByRef bufTo() As Double, ByVal size As Integer)
        For i = 0 To size - 1
            bufTo(i) = bufFrom(i)
        Next
    End Sub

    'copia un buffer de bytes
    Public Sub BufferPasteByte(ByRef bufFrom() As Byte, ByVal initFrom As Integer, ByRef bufTo() As Byte, ByVal initTo As Integer, ByVal size As Integer)
        For i = 0 To size - 1
            bufTo(initTo) = bufFrom(initFrom)
            initTo = initTo + 1
            initFrom = initFrom + 1
        Next
    End Sub

    'normalliza error de buffer
    Public Sub NormErrorBuffer(ByRef buf() As Integer, ByRef bufNorm() As Integer, ByVal maxValue As Integer, ByVal size As Integer)
        For i = 0 To size - 1
            bufNorm(i) = buf(i) * 100 / maxValue
        Next
    End Sub

    'Haya la posicion del valor maximo del buffer
    Public Function findMaxPos(ByRef buf() As Integer, ByVal size As Integer) As Integer
        Dim pos As Integer = 0
        Dim Val As Integer = 0

        For i = 0 To size - 1
            If (buf(i) > Val) Then
                Val = buf(i)
                pos = i
            End If
        Next
        Return pos
    End Function


    '--------------------------------------------------
    ' Halla el LSB de un numero
    '--------------------------------------------------
    Public Function calcLSB(ByVal numero As Integer) As Byte
        Dim lsb As Byte
        Dim msb As Byte
        Dim tmpInt As Integer

        If numero >= 65535 Then
            msb = 255
            lsb = 255
        ElseIf numero > 255 Then
            msb = Fix(numero / 256)
            tmpInt = msb
            tmpInt = tmpInt * 256
            lsb = CByte(numero - tmpInt)
        Else
            msb = 0
            lsb = CByte(numero)
        End If

        Return lsb
    End Function
    '--------------------------------------------------
    ' Halla el MSB de un numero
    '--------------------------------------------------
    Public Function calcMSB(ByVal numero As Integer) As Byte
        Dim lsb As Byte
        Dim msb As Byte
        Dim tmpInt As Integer

        If numero >= 65535 Then
            msb = 255
            lsb = 255
        ElseIf numero > 255 Then
            msb = Fix(numero / 256)
            tmpInt = msb
            tmpInt = tmpInt * 256
            lsb = CByte(numero - tmpInt)
        Else
            msb = 0
            lsb = CByte(numero)
        End If
        Return msb
    End Function
    'Alista matriz de numeros a binario
    Public Sub setMatriz2send(ByRef IntMat() As Integer, ByRef binMat() As Byte, ByVal init As Integer, ByVal size As Integer)
        Dim i As Integer

        For i = 0 To size - 1
            binMat(i + init) = IntMat(i)
        Next
    End Sub

    '----------------------------------------------------------------------------------------
    ' Haya la matriz de diferencia entre A y B, se les debe decir dessde donde empiezan a 
    ' hayar la diferencia
    '----------------------------------------------------------------------------------------
    Public Sub FindMatDif(ByRef A() As Double, ByVal A_start As Integer, ByRef B() As Double, ByVal B_start As Integer, ByRef Z() As Double, ByVal z_side As Integer)
        Dim i, j As Integer
        Dim k As Integer

        k = 0
        'Haya diferencia
        For i = 0 To z_side - 1
            For j = 0 To z_side - 1
                'Haya diferencia y lo guarda en matriz de diferencia
                Z(k) = Math.Abs(A(A_start + k) - B(B_start + k))
                k = k + 1
            Next
        Next
    End Sub

    '----------------------------------------------------------------------------------------
    ' Suma todos los puntos de una matriz 
    ' 
    '----------------------------------------------------------------------------------------
    Public Function SumAllPointsMat(ByRef A() As Double, ByVal A_start As Integer, ByVal z_side As Integer) As Double
        Dim i, j As Integer
        Dim k As Integer
        Dim result As Double
        k = 0
        result = 0
        'Haya diferencia
        For i = 0 To z_side - 1
            For j = 0 To z_side - 1
                'Haya diferencia y lo guarda en matriz de diferencia
                result = A(k) + result
                k = k + 1
            Next
        Next
        Return result
    End Function
    '----------------------------------------------------------------------------------------
    ' calcula error en la matriz
    ' 
    '----------------------------------------------------------------------------------------
    Public Function findErrorMat(ByRef A() As Double, ByVal A_start As Integer, ByVal a_side As Integer) As Double
        Dim result As Double

        result = SumAllPointsMat(A, A_start, a_side)
        result = result / (a_side * a_side)
        'result = result / ERROR_MAXIMO
        'result = result * 100
        Return result
    End Function

    '----------------------------------------------------------------------------------------
    ' Convoluciona matrices
    '----------------------------------------------------------------------------------------
    Public Sub convolMat(ByRef X() As Double, ByVal x_side As Integer, ByRef W() As Double, ByVal w_side As Integer, ByRef Z() As Double, ByVal z_side As Integer)
        Dim i, j As Integer
       
        For i = 0 To z_side - 1
            For j = 0 To z_side - 1
                'Inicializa el valor
                Z(i * z_side + j) = 0
                'calcula convolucion recorriendo kernel
                For k = 0 To w_side - 1
                    For m = 0 To w_side - 1
                        Z(i * z_side + j) = W(k * w_side + m) * X((i + k) * x_side + (j + m)) + Z(i * z_side + j)
                    Next
                Next
                'ajusta para estocastico
                Z(i * z_side + j) = Z(i * z_side + j) / (w_side * w_side)
            Next
        Next
    End Sub

    '----------------------------------------------------------------------------------------
    ' Max Pool de matriz
    '----------------------------------------------------------------------------------------
    Public Sub MPoolMat(ByRef X() As Double, ByVal x_side As Integer, ByRef Z() As Double, ByVal z_side As Integer)
        Dim i, j As Integer
        Dim tmpD As Double
        For i = 0 To z_side - 1
            For j = 0 To z_side - 1
                'selecciona el maximo entre 4 pixels circundantes
                tmpD = Math.Max(X(i * 2 * x_side + j * 2), X(i * 2 * x_side + j * 2 + 1))
                tmpD = Math.Max(tmpD, X(i * 2 * x_side + x_side + j * 2))
                tmpD = Math.Max(tmpD, X(i * 2 * x_side + x_side + j * 2 + 1))
                Z(i * z_side + j) = tmpD
            Next
        Next
    End Sub
    '----------------------------------------------------------------------------------------
    ' average Pool de matriz
    '----------------------------------------------------------------------------------------
    Public Sub AvgPoolMat(ByRef X() As Double, ByVal x_side As Integer, ByRef Z() As Double, ByVal z_side As Integer)
        Dim i, j As Integer
        Dim tmpD As Double

        For i = 0 To z_side - 1
            For j = 0 To z_side - 1
                'Average
                tmpD = X(i * 2 * x_side + j * 2) + X(i * 2 * x_side + j * 2 + 1) + X(i * 2 * x_side + x_side + j * 2) + X(i * 2 * x_side + x_side + j * 2 + 1)
                tmpD = tmpD / 4
                Z(i * z_side + j) = tmpD
            Next
        Next
    End Sub

    '----------------------------------------------------------------------------------------
    ' Multiplica matrices
    '----------------------------------------------------------------------------------------
    Public Sub multMat(ByRef A() As Double, ByVal a_side As Integer, ByRef B() As Double, ByVal b_side As Integer, ByRef Z() As Double, ByVal z_side As Integer)
        Dim i, j As Integer
        Dim k As Integer
        'Muestra la trama de resultado en TextBox 
        k = 0

        For i = 0 To z_side - 1
            For j = 0 To z_side - 1
                If (i < b_side) And (j < b_side) Then
                    Z(k) = A(k) * B(i * b_side + j)
                Else
                    Z(k) = A(k)
                End If
                k = k + 1
            Next

        Next
    End Sub


    '----------------------------------------------------------------------------------------
    ' suma matrices
    '----------------------------------------------------------------------------------------
    Public Sub sumMat(ByRef A() As Double, ByVal a_side As Integer, ByRef B() As Double, ByVal b_side As Integer, ByRef Z() As Double, ByVal z_side As Integer)
        Dim i, j As Integer
        Dim k As Integer
        'Muestra la trama de resultado en TextBox 
        k = 0

        For i = 0 To z_side - 1
            For j = 0 To z_side - 1
                If (i < b_side) And (j < b_side) Then
                    Z(k) = (A(k) + B(i * b_side + j)) / 2 ' a la suma le realiza una division por dos para que sea igual que el estocastico
                Else
                    Z(k) = -1 'aqui va el valor qu ele pone por defecto si no se suma
                End If
                k = k + 1
            Next

        Next
    End Sub

    '----------------------------------------------------------------------------------------
    ' Muestra en textBox una matriz
    '----------------------------------------------------------------------------------------
    Public Sub printMatrizInt(ByRef AnswerTrama As String, ByVal str As String, ByRef A() As Integer, ByVal A_start As Integer, ByVal z_side As Integer)
        Dim i, j As Integer
        Dim k As Integer
        'Muestra la trama de resultado en TextBox 
        k = 0
        AnswerTrama = AnswerTrama & str & vbCrLf
        For i = 0 To z_side - 1
            For j = 0 To z_side - 1
                AnswerTrama = AnswerTrama & A(A_start + k) & vbTab
                k = k + 1
            Next
            AnswerTrama = AnswerTrama & vbCrLf
        Next
        AnswerTrama = AnswerTrama & vbCrLf
    End Sub
    '----------------------------------------------------------------------------------------
    ' Muestra en textBox una matriz
    '----------------------------------------------------------------------------------------
    Public Sub printMatrizDbl(ByRef AnswerTrama As String, ByVal str As String, ByRef A() As Double, ByVal A_start As Integer, ByVal z_side As Integer)
        Dim i, j As Integer
        Dim k As Integer
        'Muestra la trama de resultado en TextBox 
        k = 0
        AnswerTrama = AnswerTrama & str & vbCrLf
        For i = 0 To z_side - 1
            For j = 0 To z_side - 1
                AnswerTrama = AnswerTrama & Math.Round(A(A_start + k), 3) & vbTab
                k = k + 1
            Next
            AnswerTrama = AnswerTrama & vbCrLf
        Next
        AnswerTrama = AnswerTrama & vbCrLf
    End Sub
    '----------------------------------------------------------------------------------------
    ' Muestra en textBox una matriz
    '----------------------------------------------------------------------------------------
    Public Sub printMatrizByte(ByRef AnswerTrama As String, ByVal str As String, ByRef A() As Byte, ByVal A_start As Integer, ByVal z_side As Integer)
        Dim i, j As Integer
        Dim k As Integer
        'Muestra la trama de resultado en TextBox 
        k = 0
        AnswerTrama = AnswerTrama & str & vbCrLf
        For i = 0 To z_side - 1
            For j = 0 To z_side - 1
                AnswerTrama = AnswerTrama & A(A_start + k) & vbTab
                k = k + 1
            Next
            AnswerTrama = AnswerTrama & vbCrLf
        Next
        AnswerTrama = AnswerTrama & vbCrLf
    End Sub


    '----------------------------------------------------------------------------------------
    ' Cnfigura para escribir una matriz en un archivo de texto
    '----------------------------------------------------------------------------------------
    Public Function setMatTxtFile(ByRef A() As Byte, ByVal a_side As Integer) As String
        Dim i, j As Integer
        Dim k As Integer
        Dim str As String = ""
        'Muestra la trama de resultado en TextBox 
        k = 0

        For i = 0 To a_side - 1
            str = str & ","
            For j = 0 To a_side - 1
                str = str & A(k) & ","
                k = k + 1
            Next
            str = str & vbCrLf
        Next
        Return str
    End Function
    

    ' Genera un numero aleatorio entre -1 y 1
    Public Function GenRandom_bip() As Double
        ' Initialize the random-number generator.
        Randomize()
        ' Generate random value between -1, 1
        Dim value As Double = (2 * Rnd()) - 1
        Return value
    End Function


    'complemento un vector
    Public Sub invVector(ByRef vec() As Byte, ByVal init As Integer, ByVal finish As Integer)
        Dim a As Integer
       
        For i = init To finish
            a = vec(i)
            a = 255 - a
            vec(i) = CByte(a)
        Next
    End Sub

    'Compara dos buffers
    Public Function compareBuffer(ByRef B1() As Byte, ByVal init1 As Integer, ByRef B2() As Byte, ByVal init2 As Integer, ByVal size As Integer) As Boolean

        For i = 0 To size - 1
            If (B1(i + init1) <> B2(init2 + i)) Then
                Return False
            End If
        Next
        Return True
    End Function

    ' obtiene el label de el vector de bytes que envia la FPGA como resutlado
    ' 
    Public Function getLabelfromVectorBytes(ByRef v() As Byte, ByVal init As Integer) As Integer
        'Dim j As Integer = 0
        Dim max As Byte = 0
        Dim label As Integer = 255

        For i = 0 To 9
            If (v(init + i) > max) Then
                max = v(init + i)
                label = i
            End If
        Next
        Return label
    End Function

End Module

# VISUAL BASIC TOOL para DEMO VALIDA FPGA   
Esta herramienta en VBasic sirve para poder realizar la demo de la FPGA con el proyecto VALIDA (https://gitlab.com/valida_project/valida_fpga_demo). 

La idea es que captura datos por la UART cafa vez que un evento es detectado. Para más info, ver: https://gitlab.com/valida_project/valida_fpga_demo/-/blob/master/_doc/DEMO%20VALIDA_OUT_CSV.pdf?ref_type=heads

